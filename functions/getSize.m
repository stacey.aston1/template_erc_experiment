function stimulusWidth = getSize(distanceToScreen,depthToSimulate,sizeAtBackground)
% this doesn't do anything that different to the getAmplitude script but is
% re-written here with different terminology for clarity
% size at background is the width of the stimulus square (in degrees of
% visual angle) when it is in the plane of the background

% get the change in distance
deltaD = depthToSimulate - distanceToScreen;
% get the size at background in cm
sizeAtBackgroundCM = 2*distanceToScreen*tand(sizeAtBackground/2);
% get the size at the required depth in degrees of visual angle
stimulusWidth = 2*atand(sizeAtBackgroundCM/(2*(distanceToScreen+deltaD)));

end