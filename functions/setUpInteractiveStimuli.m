function [data,params] = setUpInteractiveStimuli(params,part)

% general stimulus presentation parameters (This will be needed for all
% tasks using the showStimulus function but may change across tasks)
params.timeLimit = 5; % response time limit (in seconds), end if no input for 1 minute
params.waitframes = 4; % how many frames to wait for good timing
params.numFrames = round(params.timeLimit/(params.ifi*params.waitframes)); % how many frames we will show in the time limit
params.times = linspace(0,params.timeLimit,params.numFrames); % the time that each frame corresponds to (in seconds)
% whether or not to check for a response during stimulus presentation, we
% might not want to if only showing for set time (e.g., 2IFC task), or we
% might leave the stimulus on screen until response (or timed out)
params.checkResponse = 0;
% whether to show stimulus information on the screen
params.showInfo = 1;

% add some keys we need for this task
params.keyIDs{2,1} = 'increase';
params.keyInd(2,1) = KbName('UpArrow'); 
params.keyIDs{3,1} = 'decrease';
params.keyInd(3,1) = KbName('DownArrow'); 
params.keyIDs{4,1} = 'left';
params.keyInd(4,1) = KbName('LeftArrow'); 
params.keyIDs{5,1} = 'right';
params.keyInd(5,1) = KbName('RightArrow'); 

% how much to change the depth each time
data.depthStep = 1; % in cm

% random starting depth due to disparity
data.disparityDepth = round(params.distance-10+rand*20,0); % in cm

% start with congruent cues 
data.sizeDepth = data.disparityDepth;
data.textureDepth = data.disparityDepth;
data.motionDepth = data.disparityDepth;
data.audioDepth = data.disparityDepth;

% start with all cues present
data.disparityPresent = 0;
data.sizePresent = 1;
data.texturePresent = 1;
data.motionPresent = 0;
data.audioPresent = 0;

% track which stimulus parameter is being toggled
data.toggleIDs = {...
    'All Scroll'
    'Disparity Scroll'
    'Size Scroll'
    'Texture Scroll'
    'Motion Scroll'
    'Sound Scroll'
    'Disparity Present'
    'Size Present'
    'Texture Present'
    'Motion Present'
    'Sound Present'
    'Dot Density'
    'Background Size'
    'Stimulus Size'
    'Distance To Screen'
    'Dot Diameter'
    'Gap Width'
    'Disparity Jitter'
    };
data.toggle = [1;zeros(length(data.toggleIDs)-1,1)];

% attached participant info to data structure
data.part = part;

end





