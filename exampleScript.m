% Clear the workspace
clc; clear; close all;
sca;
addpath(genpath('functions'))
try
    PsychPortAudio('Close');
catch
    disp('Tried to close PsychPortAudio but not open')
end

%% ----- SET UP PSYCHTOOLBOX -----
% This is unlikely to change across experiments

% set a steromode
params.stereoMode = 8; % for red-blue, but we will fine tune later

% Some default settings
PsychDefaultSetup(2);
Screen('Preference', 'SkipSyncTests', 1);

% Setup Psychtoolbox for OpenGL 3D rendering support and initialize the
% mogl OpenGL for Matlab wrapper
InitializeMatlabOpenGL;

% Random number generator
rng('shuffle');

% Get the screen numbers. This gives us a number for each of the screens
% attached to our computer.
params.screens = Screen('Screens');
  
% Defines the screen to use
params.screenNumber = max(params.screens);
 
% Open a window
[params.window, params.windowRect] = PsychImaging('OpenWindow', params.screenNumber, ...
    0, [], 32, 2, params.stereoMode);

% Get the size of the on screen window in pixels
[params.screenXpixels, params.screenYpixels] = Screen('WindowSize', params.window);

% Get the centre coordinate of the window in pixels
[params.xCenter, params.yCenter] = RectCenter(params.windowRect);

% fine tune the colour gains
SetAnaglyphStereoParameters('LeftGains', params.window, [0.4 0.0 0.0]);
SetAnaglyphStereoParameters('RightGains', params.window, [0.0 0.2 0.7]);

% Set up alpha-blending for smooth (anti-aliased) edges to our dots
Screen('BlendFunction', params.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% set up a quit button
KbName('UnifyKeyNames');
params.keyIDs{1,1} = 'quit';
params.keyInd(1,1) = KbName('ESCAPE'); 
[~,~,keyCode] = KbCheck;
 
% Hide the cursor   
HideCursor; 

% text settings
Screen('TextFont', params.window, 'Ariel');
Screen('TextSize', params.window, 30);

% Measure the vertical refresh rate of the monitor
params.ifi = Screen('GetFlipInterval', params.window);

%% ----- GET GENERAL STIMULUS PARAMETERS -----
% not all tasks will show all stimuli but we will get the all
% stimulus parameters from the same function for all tasks in the long-term
% training study to avoid errors

params = getStimulusParameters(params);

%% ----- AUDIO SET UP -----

% audio set up
InitializePsychSound(1);
params.audioHandle = PsychPortAudio('Open', [], [], 2, params.Fs, 1);

%% ----- GET INFORMATION ABOUT THE PARTICIPANT -----
% we will not need all this input in the long-term training study as we can
% load from previous data files but we will need all this in lead up
% studies

Screen('FillRect', params.window, [0.3 0.3 0.3]);
part.pID = GetEchoString(params.window, 'Enter Participant ID:',...
    params.screenXpixels*0.35,params.screenYpixels*0.35,params.textColour,[0.3 0.3 0.3],0,2,[],[]);
part.age = GetEchoString(params.window, 'Enter Participant Age:',...
    params.screenXpixels*0.35,params.screenYpixels*0.45,params.textColour,[0.3 0.3 0.3],0,2,[],[]);
part.sex = GetEchoString(params.window, 'Enter Participant Sex:',...
    params.screenXpixels*0.35,params.screenYpixels*0.55,params.textColour,[0.3 0.3 0.3],0,2,[],[]);
part.interocularDistance = str2double(GetEchoString(params.window, 'Enter Interocular Distance:',...
    params.screenXpixels*0.35,params.screenYpixels*0.65,params.textColour,[0.3 0.3 0.3],0,2,[],[]));
part.date = datestr(now);

%% ----- RUN THE EXPERIMENT -----
% This will be replaced with a big if statement in long-term training study
% where we decide what task the participant should do in this session

% The example script shows the example interactive stimuli here. Look at
% this script to see how the code to convert required depths to
% displacements, disparities, and sounds works and how to present the
% stimulus with the "showStimulus" function
data = runInteractiveStimuli(params,part); 

%% ----- END AND TIDY UP -----

text = 'Thanks! The task is now complete. We are very grateful for your time.';

Screen('FillRect', params.window, [0.3 0.3 0.3]);
DrawFormattedText(params.window, text, 'center', 'center', params.textColour);
Screen('Flip', params.window);
pause(3)

% save the data
% save(['memoryDumps\TaskXXXDump_',pID,'_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')])
% save(['data\TaskXXXData_',pID],'data','params')

% tidy up
PsychPortAudio('Close');
sca;

