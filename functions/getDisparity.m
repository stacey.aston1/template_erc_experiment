function offsets = getDisparity(distanceToScreen,depthsToSimulate,interocularDistance)

% calculate the offsets that we need in cm for these disparity depths
deltaDs = depthsToSimulate - distanceToScreen;
offsets = (interocularDistance .* deltaDs) ./ (2 .* (deltaDs + distanceToScreen));

end
