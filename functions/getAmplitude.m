function amplitude = getAmplitude(distanceToScreen,depthToSimulate,backgroundAmplitude)

% get the change in distance
deltaD = depthToSimulate - distanceToScreen;
% get the background amplitude in cm
backgroundAmpCM = 2*distanceToScreen*tand(backgroundAmplitude/2);
% get the stimulus amplitude in degrees of visual angle
amplitude = 2*atand(backgroundAmpCM/(2*(distanceToScreen+deltaD)));

end