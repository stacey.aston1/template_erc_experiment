function keyCode = getResponse(params)

text = 'Respond now please, muchly, thank you!';

Screen('FillRect', params.window, [0.3 0.3 0.3]);
DrawFormattedText(params.window, text, 'center', 'center', params.textColour);
Screen('Flip', params.window);

% make sure there are no lingering early presses
[~,~,keyCode] = KbCheck;
while any(keyCode(params.keyInd))
    % check for a key press
    [~,~,keyCode] = KbCheck;
end

% get the meaningful press
while ~any(keyCode(params.keyInd))
    % check for a key press
    [~,~,keyCode] = KbCheck;
end

end