 function [waveform, outputFreq, L_R, L_T_raw, L_T_mod] = getWaveform(semitoneDiff, params)

% Configure sound settings
amp = 0.5;              % Sound amplitude.
Ts =  1/params.Fs;      % Sampling for output time vector 
secs = 0:Ts:params.timeLimit;   % Time vector

% Turn angular frequency to radians for the reference
rad_semitones_ref = 2*pi*params.meanFreq; 

% Make the reference waveform
waveformR = amp*sin(rad_semitones_ref*secs);

% Get percieved ref loudness
L_R = acousticLoudness(waveformR',params.Fs);

% Define frequency [Hz] of test stimuli
outputFreq = 2^(semitoneDiff/12)*params.meanFreq;

% Turn angular frequency to radians for test
rad_semitones_test = 2*pi*outputFreq; 

% Create test waveform
waveformT = amp*sin(rad_semitones_test*secs);

% Get percieved raw test loudness
L_T_raw = acousticLoudness(waveformT',params.Fs);

% Define the function we want to minimise to find the amplitude modifier
fun = @(alpha) (acousticLoudness(alpha*waveformT',params.Fs) - L_R)^2;

% Solve for alpha
alphaHat = fminsearch(fun,1);

% Modified loudness
L_T_mod = acousticLoudness(alphaHat*waveformT',params.Fs);

% Get the scaled waveform
waveform = alphaHat*waveformT;

end