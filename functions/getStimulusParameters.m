function params = getStimulusParameters(params)

% appearance parameters
params.textColour = [1 1 1];
params.dotColour = [1 1 1];

% distance from screen
params.distance = 50; % (distance for behavioural stuff in Dekker et al., 2015)

% screen height and width
params.height = 28.6; % in cm
params.width = 50.9; % in cm

% screen width in dov
params.widthD = 2*atand(params.width/(2*params.distance));

% how many pixels per degree?
params.pixelsPerDegree = params.screenXpixels/params.widthD;

% stimulus square in dov and converted to pixels
params.stimWidthD = 11; % (Dekker et al., 2015)
params.stimWidth = params.pixelsPerDegree*params.stimWidthD;
params.stimHeight = params.stimWidth;

% background rectangle in dov and converted to pixels
params.backWidthD = 20; % (Dekker et al., 2015)
params.backWidth = params.pixelsPerDegree*params.backWidthD;
params.backHeightD = 16; % (Dekker et al., 2015)
params.backHeight = params.pixelsPerDegree*params.backHeightD;

% gap between background and stimulus square
params.gapD = 1;
params.gap = params.pixelsPerDegree*params.gapD;

% how much to jitter each dot in depth when showing disparity (in cm)
% define SD of the Gaussian noise
params.sdJit = 1; 

% background dot amplitude (in dov and converted)
params.backDisAmpD = 0.5; % (Dekker et al., 2015)
params.backDisAmp = params.pixelsPerDegree*params.backDisAmpD;

% required dot density (dots per square degree)
params.requiredDensity = 15; % (Preston et al, 2008; ref 18 in Ban et al, 2012)

% how many dots do we need for required density?
params.nDots = round(params.backWidthD*params.backHeightD*params.requiredDensity);

% dot diameter (in dov and converted)
params.dotDiamD = 0.2;
params.dotDiam = params.pixelsPerDegree*params.dotDiamD;

% fixation cross line lengths (in dov and converted)
params.fixationD = 0.5;
params.fixation = params.pixelsPerDegree*params.fixationD;

% mean audio frequency (mapped to background, or depth of screen)
params.meanFreq = 600;

% Samples per second. Should at least double the max frequency. 
% 44.1kHz is also okay.  
params.Fs = 48000;             

end













