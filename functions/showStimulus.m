function keyCode = showStimulus(params,disparity,size,texture,motion,...
    audio,disparityDepth,sizeDepth,textureDepth,motionDepth,audioDepth,...
    interocular,toggle)

% get the left/right eye offset if showing disparity
if disparity
    % get the noisy dot depths
    disparityDepths = (randn(1,params.nDots) .* params.sdJit) + disparityDepth;
    % get the offset for each dot
    offsets = getDisparity(params.distance,disparityDepths,interocular);
    % convert offsets to pixels
    rightShiftPix = (offsets ./ params.width) .* params.screenXpixels;
else
    rightShiftPix = zeros(1,params.nDots);
end

% get the change in the stimulus square size if showing size
if size
    stimulusWidthD = getSize(params.distance,sizeDepth,params.stimWidthD);
    % convert to pixels
    stimulusWidth = params.pixelsPerDegree*stimulusWidthD;
else
    stimulusWidth = params.stimWidth;
end

% get the change in the stimulus dot size if showing texture
if texture
    dotDiameterD = getSize(params.distance,textureDepth,params.dotDiamD);
    % convert to pixels
    dotDiameter = params.pixelsPerDegree*dotDiameterD;
else
    dotDiameter = params.dotDiam;
end

% get the amplitude if showing motion
if motion
    amplitudeD = getAmplitude(params.distance,motionDepth,params.backDisAmpD);
    % convert to pixels
    amplitude = params.pixelsPerDegree*amplitudeD;
end

% get the waveform if playing sound
if audio
    semitoneDiff = getSemitoneDifference(params.distance,audioDepth);
    waveform = getWaveform(semitoneDiff,params);
    PsychPortAudio('FillBuffer', params.audioHandle, waveform);
end

% get random dot positions
dotPosX = rand(1,params.nDots)*params.backWidth - params.backWidth/2;
dotPosY = rand(1,params.nDots)*params.backHeight - params.backHeight/2;

% pull out background dots
backDotInds = find(-(params.stimWidth/2 + params.gap) > dotPosX | ...
    dotPosX > params.stimWidth/2  + params.gap | ...
    -(params.stimWidth/2  + params.gap) > dotPosY | ...
    dotPosY > params.stimWidth/2  + params.gap);
dotPosXback = dotPosX(backDotInds);
dotPosYback = dotPosY(backDotInds);

% find all dots stimulus dots (removing central square for fixation) and shift
stimDotInds = find((-stimulusWidth/2 <= dotPosX & ...
    dotPosX <= stimulusWidth/2 & ...
    -stimulusWidth/2 <= dotPosY & ...
    dotPosY <= stimulusWidth/2) & ...
    ~(-params.fixation <= dotPosX & ...
    dotPosX <= params.fixation & ...
    -params.fixation <= dotPosY & ...
    dotPosY <= params.fixation));
dotPosXleft = dotPosX(stimDotInds) - rightShiftPix(stimDotInds);
dotPosXright = dotPosX(stimDotInds) + rightShiftPix(stimDotInds);
dotPosYstim = dotPosY(stimDotInds);

% if showing motion, get the dot displacements
if motion
    % get the background dot displacements
    backDisplacements = params.backDisAmp*sin(2*pi*repmat(params.times',[1,length(dotPosXback)]));
    % get the stimulus dot displacements
    stimDisplacements = amplitude*sin(2*pi*repmat(params.times',[1,length(dotPosYstim)]));
else
    backDisplacements = zeros(length(params.times),length(dotPosXback));
    stimDisplacements = zeros(length(params.times),length(dotPosYstim));
end

% loop stimulus until a button is pressed
[~,~,keyCode] = KbCheck;
vbl = Screen('Flip', params.window, [], 1);
frame = 0;
if audio
    PsychPortAudio('Start', params.audioHandle, 1, 0);
end
while (params.checkResponse && (~any(keyCode(params.keyInd)) && frame < params.numFrames)) || ...
    (~params.checkResponse && frame < params.numFrames)
    
    % count the frames
    frame = frame + 1;
    
    % Select left-eye image buffer for drawing (buffer = 0)
    Screen('SelectStereoDrawBuffer', params.window, 0);
    
    % draw common stimuli for left eye
    drawCommonStimuli(params,dotPosXback,dotPosYback,disparity,size,texture,motion,...
        audio,disparityDepth,sizeDepth,textureDepth,motionDepth,audioDepth,...
        backDisplacements,frame,toggle);
    
    % draw left dots
    Screen('DrawDots', params.window, [dotPosXleft + stimDisplacements(frame,:);...
        dotPosYstim], dotDiameter, params.dotColour, [params.screenXpixels/2,...
        params.screenYpixels/2], 2);
    
    % Select right-eye image buffer for drawing (buffer = 1)
    Screen('SelectStereoDrawBuffer', params.window, 1);
    
    % draw common stimuli for right eye
    drawCommonStimuli(params,dotPosXback,dotPosYback,disparity,size,texture,motion,...
        audio,disparityDepth,sizeDepth,textureDepth,motionDepth,audioDepth,...
        backDisplacements,frame,toggle);
    
    % draw right dots
    Screen('DrawDots', params.window, [dotPosXright + stimDisplacements(frame,:);...
        dotPosYstim], dotDiameter, params.dotColour, [params.screenXpixels/2,...
        params.screenYpixels/2], 2);
    
    % show it
    vbl = Screen('Flip', params.window, vbl + (params.waitframes - 0.5) * params.ifi);

    % check for a key press
    [~,~,keyCode] = KbCheck;
    
end
if audio
    PsychPortAudio('Stop', params.audioHandle);
end

end

function drawCommonStimuli(params,dotPosXback,dotPosYback,disparity,size,texture,motion,...
    audio,disparityDepth,sizeDepth,textureDepth,motionDepth,audioDepth,backDisplacements,frame,toggle)

% draw fixation cross
Screen('DrawLine',params.window,params.dotColour, ...
    params.xCenter-params.fixation/2, params.yCenter, ...
    params.xCenter+params.fixation/2, params.yCenter, ...
    params.dotDiam/2)
Screen('DrawLine',params.window,params.dotColour, ...
    params.xCenter, params.yCenter-params.fixation/2, ...
    params.xCenter, params.yCenter+params.fixation/2, ...
    params.dotDiam/2)

% draw background dots
Screen('DrawDots', params.window, [dotPosXback + backDisplacements(frame,:);...
    dotPosYback], params.dotDiam,params.dotColour,[params.screenXpixels/2,...
    params.screenYpixels/2], 2);

% write the stimulus parameters on the screen if required and show which
% one can be toggled
if params.showInfo
    
    % show true depths for each cue
    disparityText = ['Disparity:\n\n',num2str(disparityDepth),' cm'];
    sizeText = ['Size:\n\n',num2str(sizeDepth),' cm'];
    textureText = ['Texture:\n\n',num2str(textureDepth),' cm'];
    motionText = ['Motion:\n\n',num2str(motionDepth),' cm'];
    audioText = ['Audio:\n\n',num2str(audioDepth),' cm'];
    DrawFormattedText(params.window, disparityText,...
        0.08*params.screenXpixels, 0.15*params.screenYpixels,...
        params.textColour*(toggle(2,1)+toggle(1,1)) + [0.5 0.5 0.5]*(1-toggle(2,1)-toggle(1,1)));
    DrawFormattedText(params.window, sizeText,...
        0.28*params.screenXpixels, 0.15*params.screenYpixels,...
        params.textColour*(toggle(3,1)+toggle(1,1)) + [0.5 0.5 0.5]*(1-toggle(3,1)-toggle(1,1)));
    DrawFormattedText(params.window, textureText,...
        0.48*params.screenXpixels, 0.15*params.screenYpixels,...
        params.textColour*(toggle(4,1)+toggle(1,1)) + [0.5 0.5 0.5]*(1-toggle(4,1)-toggle(1,1)));
    DrawFormattedText(params.window, motionText,...
        0.68*params.screenXpixels, 0.15*params.screenYpixels,...
        params.textColour*(toggle(5,1)+toggle(1,1)) + [0.5 0.5 0.5]*(1-toggle(5,1)-toggle(1,1)));
    DrawFormattedText(params.window, audioText,...
        0.88*params.screenXpixels, 0.15*params.screenYpixels,...
        params.textColour*(toggle(6,1)+toggle(1,1)) + [0.5 0.5 0.5]*(1-toggle(6,1)-toggle(1,1)));
    
    % show whether each cue if present
    disparityPresentText = ['Disparity Present: ',num2str(disparity)];
    sizePresentText = ['Size Present: ',num2str(size)];
    texturePresentText = ['Texture Present: ',num2str(texture)];
    motionPresentText = ['Motion Present: ',num2str(motion)];
    audioPresentText = ['Audio Present: ',num2str(audio)];
    DrawFormattedText(params.window, disparityPresentText,...
        0.05*params.screenXpixels, 0.3*params.screenYpixels,...
        params.textColour*(toggle(7,1)) + [0.5 0.5 0.5]*(1-toggle(7,1)));
    DrawFormattedText(params.window, sizePresentText,...
        0.05*params.screenXpixels, 0.4*params.screenYpixels,...
        params.textColour*(toggle(8,1)) + [0.5 0.5 0.5]*(1-toggle(8,1)));
    DrawFormattedText(params.window, texturePresentText,...
        0.05*params.screenXpixels, 0.5*params.screenYpixels,...
        params.textColour*(toggle(9,1)) + [0.5 0.5 0.5]*(1-toggle(9,1)));
    DrawFormattedText(params.window, motionPresentText,...
        0.05*params.screenXpixels, 0.6*params.screenYpixels,...
        params.textColour*(toggle(10,1)) + [0.5 0.5 0.5]*(1-toggle(10,1)));
    DrawFormattedText(params.window, audioPresentText,...
        0.05*params.screenXpixels, 0.7*params.screenYpixels,...
        params.textColour*(toggle(11,1)) + [0.5 0.5 0.5]*(1-toggle(11,1)));
    
    % show general stimulus parameters
    densityText = ['Dot Density: ',num2str(params.requiredDensity)];
    backgroundText = ['Background Size: ',num2str(params.backWidthD),' x ',num2str(params.backHeightD)];
    stimulusText = ['Stimulus Size: ',num2str(params.stimWidthD),' x ',num2str(params.stimWidthD)];
    distanceText = ['Distance To Screen: ',num2str(params.distance),' cm'];
    dotSizeText = ['Dot Diameter: ',num2str(params.dotDiamD)];
    gapText = ['Gap Width: ',num2str(params.gapD)];
    jitText = ['Disparity Jitter: ',num2str(params.sdJit)];
    DrawFormattedText(params.window, densityText,...
        0.75*params.screenXpixels, 0.3*params.screenYpixels,...
        params.textColour*(toggle(12,1)) + [0.5 0.5 0.5]*(1-toggle(12,1)));
    DrawFormattedText(params.window, backgroundText,...
        0.75*params.screenXpixels, 0.4*params.screenYpixels,...
        params.textColour*(toggle(13,1)) + [0.5 0.5 0.5]*(1-toggle(13,1)));
    DrawFormattedText(params.window, stimulusText,...
        0.75*params.screenXpixels, 0.5*params.screenYpixels,...
        params.textColour*(toggle(14,1)) + [0.5 0.5 0.5]*(1-toggle(14,1)));
    DrawFormattedText(params.window, distanceText,...
        0.75*params.screenXpixels, 0.6*params.screenYpixels,...
        params.textColour*(toggle(15,1)) + [0.5 0.5 0.5]*(1-toggle(15,1)));
    DrawFormattedText(params.window, dotSizeText,...
        0.75*params.screenXpixels, 0.7*params.screenYpixels,...
        params.textColour*(toggle(16,1)) + [0.5 0.5 0.5]*(1-toggle(16,1)));
    DrawFormattedText(params.window, gapText,...
        0.75*params.screenXpixels, 0.8*params.screenYpixels,...
        params.textColour*(toggle(17,1)) + [0.5 0.5 0.5]*(1-toggle(17,1)));
    DrawFormattedText(params.window, jitText,...
        0.75*params.screenXpixels, 0.9*params.screenYpixels,...
        params.textColour*(toggle(18,1)) + [0.5 0.5 0.5]*(1-toggle(18,1)));

end

end
















