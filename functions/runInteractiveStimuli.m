function data = runInteractiveStimuli(params,part)
% This functions serves two purposes. First, it is a place holder in the
% template experiment script. Secondly, it is a demonstration of the native
% and novel cues to depth that we use, allowing the observer to scroll
% through different depths

% ----- SET UP TRIALS -----
% We don't need this for the interactive stimuli but for the different
% tasks we may have something here that either defines all trials that will
% be seen ahead of time (e.g., for method of constant stimuli tasks) or
% that defines parameters of an adapative procedure (e.g., staircase
% starting values, step sizes, termination rules, etc.)
% this function should define the data structure, with the participant
% info (the part structure) being attached to it (i.e., define data.part =
% part and define some combination of data.trials, data.staircaseParams,
% etc.)
% [data,params] = setUpTaskXXX(params,part);
[data,params] = setUpInteractiveStimuli(params,part);

% ----- EXPERIMENT LOOP -----
% parts of this experiment loop (e.g., getting the offset etc.) will be
% useful in all experiments and can be recylced

[~,~,keyCode] = KbCheck;
while ~keyCode(params.keyInd(1,1)) % until the quit key is hit
    
    % ----- PROCESS CUE INFORMATION -----
    
    % find out which cues to display
    disparity = data.disparityPresent;
    size = data.sizePresent;
    texture = data.texturePresent;
    motion = data.motionPresent;
    audio = data.audioPresent;
    
    % ----- SHOW THE STIMULUS -----
    
    % show the stimulus
    keyCode = showStimulus(params,disparity,size,texture,motion,audio,...
        data.disparityDepth,data.sizeDepth,data.textureDepth,...
        data.motionDepth,data.audioDepth,...
        part.interocularDistance,data.toggle);

    % ----- GET A RESPONSE IF DIDN'T GET ONE WHILE SHOWING STIMULUS -----

    if ~params.checkResponse
        keyCode = getResponse(params);
    end
    
    % ----- PROCESS RESPONSE -----
    
    % find out what the button press was and react to it
    if ~keyCode(params.keyInd(1,1)) % only check other presses if the quit button wasn't hit
        % if the left or right key was hit, change what is being toggled
        if keyCode(params.keyInd(4,1)) || keyCode(params.keyInd(5,1))
            toggleInd = find(data.toggle);
            if keyCode(params.keyInd(4,1))
                toggleInd = toggleInd - 1;
            else
                toggleInd = toggleInd + 1;
            end
            if toggleInd < 1
                toggleInd = length(data.toggleIDs);
            end
            if toggleInd > length(data.toggleIDs)
                toggleInd = 1;
            end
            data.toggle = zeros(length(data.toggleIDs),1);
            data.toggle(toggleInd,1) = 1;
        else % otherwise, we change something about the stimulus
            % toggle disparity depth?
            if data.toggle(1,1) || data.toggle(2,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    data.disparityDepth = data.disparityDepth + data.depthStep;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    data.disparityDepth = data.disparityDepth - data.depthStep;
                end
            end
            % toggle size depth?
            if data.toggle(1,1) || data.toggle(3,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    data.sizeDepth = data.sizeDepth + data.depthStep;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    data.sizeDepth = data.sizeDepth - data.depthStep;
                end
            end
            % toggle texture depth?
            if data.toggle(1,1) || data.toggle(4,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    data.textureDepth = data.textureDepth + data.depthStep;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    data.textureDepth = data.textureDepth - data.depthStep;
                end
            end
            % toggle motion depth?
            if data.toggle(1,1) || data.toggle(5,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    data.motionDepth = data.motionDepth + data.depthStep;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    data.motionDepth = data.motionDepth - data.depthStep;
                end
            end
            % toggle audio depth?
            if data.toggle(1,1) || data.toggle(6,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    data.audioDepth = data.audioDepth + data.depthStep;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    data.audioDepth = data.audioDepth - data.depthStep;
                end
            end
            % toggle disparity presence?
            if data.toggle(7,1) && (keyCode(params.keyInd(2,1)) || keyCode(params.keyInd(3,1)))
                data.disparityPresent = 1-data.disparityPresent;
            end
            % toggle size presence?
            if data.toggle(8,1) && (keyCode(params.keyInd(2,1)) || keyCode(params.keyInd(3,1)))
                data.sizePresent = 1-data.sizePresent;
            end
            % toggle texture presence?
            if data.toggle(9,1) && (keyCode(params.keyInd(2,1)) || keyCode(params.keyInd(3,1)))
                data.texturePresent = 1-data.texturePresent;
            end
            % toggle motion presence?
            if data.toggle(10,1) && (keyCode(params.keyInd(2,1)) || keyCode(params.keyInd(3,1)))
                data.motionPresent = 1-data.motionPresent;
            end
            % toggle audio presence?
            if data.toggle(11,1) && (keyCode(params.keyInd(2,1)) || keyCode(params.keyInd(3,1)))
                data.audioPresent = 1-data.audioPresent;
            end
            % toggle dot density?
            if data.toggle(12,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.requiredDensity = params.requiredDensity + 1;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.requiredDensity = params.requiredDensity - 1;
                end
                params.nDots = round(params.backWidthD*params.backHeightD*params.requiredDensity);
            end
            % toggle background size?
            if data.toggle(13,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.backWidthD = params.backWidthD + 1;
                    params.backHeightD = round(params.backWidthD*0.8);
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.backWidthD = params.backWidthD - 1;
                    params.backHeightD = round(params.backWidthD*0.8);
                end
                 params.backWidth = params.pixelsPerDegree*params.backWidthD;
                 params.backHeight = params.pixelsPerDegree*params.backHeightD;
            end
            % toggle stimulus size?
            if data.toggle(14,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.stimWidthD = params.stimWidthD + 1;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.stimWidthD = params.stimWidthD - 1;
                end
                 params.stimWidth = params.pixelsPerDegree*params.stimWidthD;
            end
            % toggle distance to screen?
            if data.toggle(15,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.distance = params.distance + 1;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.distance = params.distance - 1;
                end
                % redefined everthing as all sizes (in terms of dov) have
                % changed
                params.widthD = 2*atand(params.width/(2*params.distance));
                params.pixelsPerDegree = params.screenXpixels/params.widthD;
                params.stimWidth = params.pixelsPerDegree*params.stimWidthD;
                params.stimHeight = params.stimWidth;
                params.backWidth = params.pixelsPerDegree*params.backWidthD;
                params.backHeight = params.pixelsPerDegree*params.backHeightD;
                params.backDisAmp = params.pixelsPerDegree*params.backDisAmpD;
                params.nDots = round(params.backWidthD*params.backHeightD*params.requiredDensity);
                params.dotDiam = params.pixelsPerDegree*params.dotDiamD;
            end
            % toggle dot diameter?
            if data.toggle(16,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.dotDiamD = params.dotDiamD + 0.01;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.dotDiamD = params.dotDiamD - 0.01;
                end
                params.dotDiam = params.pixelsPerDegree*params.dotDiamD;
            end
            % toggle gap width?
            if data.toggle(17,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.gapD = params.gapD + 0.05;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.gapD = params.gapD - 0.05;
                end
                params.gap = params.pixelsPerDegree*params.gapD;
            end
            % toggle disparity jitter?
            if data.toggle(18,1)
                if keyCode(params.keyInd(2,1)) % increase?
                    params.sdJit = params.sdJit + 0.1;
                elseif keyCode(params.keyInd(3,1)) % decrease?
                    params.sdJit = params.sdJit - 0.1;
                end
            end
        end
    end
    
    % check that the keys are released (unless quit key was hit)
    if ~keyCode(params.keyInd(1,1))
        while any(keyCode(params.keyInd))
            [~,~,keyCode] = KbCheck;
        end
    end
    
end









